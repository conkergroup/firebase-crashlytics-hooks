const fs = require('fs');
const path = require('path');
const execSync = require('child_process').execSync;

const functionsDir = path.join(__dirname, 'functions');

const fileEncoding = 'utf-8';
const firebaseSettingsPath = path.join(__dirname, '.firebaserc');
const deployStatePath = path.join(__dirname, 'deploy_state.json');


const functionsPackage = require('./functions/package.json');



const shouldForceDeployment = process.argv.indexOf('--force') !== -1;
const shouldActuallyRun = process.argv.indexOf('--test') === -1;
const shouldInheritStream = process.argv.indexOf('--verbose') !== -1;

const stdio = shouldInheritStream ? 'inherit' : null;

// Load the deployed projects states
var deployedProjects = {};
if(fs.existsSync(deployStatePath)) {
	const deployStateBuffer = fs.readFileSync(deployStatePath, fileEncoding);
	deployedProjects = JSON.parse(deployStateBuffer);
}

async function deployProject(projectAlias) {
	try {
		// Start by loading the firebase configuration file
		const firebaseConfigString = fs.readFileSync(firebaseSettingsPath, fileEncoding);
		const firebaseConfig = JSON.parse(firebaseConfigString);
		const firebaseProjects = firebaseConfig.projects;

		// Don't push new functions
		const projectId = firebaseProjects[projectAlias];			
		const deployedVersion = deployedProjects[projectAlias];
		if(!deployedVersion || deployedVersion !== functionsPackage.version || shouldForceDeployment) {
			if(shouldActuallyRun) {
				// Switch to the project and then deploy functions only
				console.log(`Deploying functions for "${projectAlias}"...`);
				await execSync('firebase use ' + projectAlias, { 'stdio': stdio, 'cwd': __dirname });
				await execSync('firebase deploy --force --only functions', { 'stdio': stdio, 'cwd': functionsDir });
				deployedProjects[projectAlias] = functionsPackage.version;
				console.log(`Functions "${functionsPackage.version}" for "${projectAlias}" have been deployed.`);
			} else {
				console.log(`Skipping "${projectAlias}", because argument "--try" is present.`)
			}
		} else {
			console.log(`Skipping "${projectAlias}", because is already at version ${deployedVersion}.`);
		}
	} catch(deployFunctionsException) {
		// Report the issue to the console
		console.log(`Failed to deploy "${projectAlias}":`);
		console.dir(deployFunctionsException);
	}	
}

/**
 * We are going to deploy to all specified project aliases.
 */
async function deploy() {
	// Start by loading the firebase configuration file
	const firebaseConfigString = fs.readFileSync(firebaseSettingsPath, fileEncoding);
	const firebaseConfig = JSON.parse(firebaseConfigString);
	const firebaseProjects = Object.keys(firebaseConfig.projects);
	for(let i = 0; i < firebaseProjects.length; i++) {
		await deployProject(firebaseProjects[i]);
	}
}

/**
 * Execute deployment strategy.
 */
(async function() {
	try {
		await deploy();		
	} catch(deployFunctionsException) {
		// Report the issue to the console
		console.log('Caught exception attempting to deploy functions.');
		console.dir(deployFunctionsException);
	} finally {
		// Always write out the deployed function versions
		const deployStateBuffer = JSON.stringify(deployedProjects);
		fs.writeFileSync(deployStatePath, deployStateBuffer, fileEncoding);
		console.log('Deployment finished with state:');
		console.dir(deployedProjects);
	}
})();








