# Firebase Crash Handler

The Firebase Crash Handler (FCH) is a set of Firebase Functions that direct new, regressed and velocity issue alerts towards [UP!](up.conker.cloud) From there, issues will be turned into a Basecamp Task and assignees notified. 

The script works on multiple Firebase projects and functions are deployed to each project / application. Note that Firebase requires a **paid service account** to enable Cloud Functions that make network requests outside of Google's services.

## Getting Started

### Firebase Command Line Tools

The very first thing you'll need are the Firebase CLI tools:

```bash
npm install -g firebase-tools
```

Then you need to login to the CLI tools in order for them to be useful. 

```bash
firebase login
```

You must authenticate with an account that has administrator access to all of the available projects, otherwise deployments may fail in unexpected ways. 

### Workspace Setup

Setting up the workspace is straight-forward:

```bash
git clone git@bitbucket.org:conkergroup/firebase-crashlytics-hooks.git
cd firebase-crashlytics-hooks
npm install
```



- `.firebaserc` contains a list of projects to deploy to. Commit all changes to this file.
- `deploy_state.json` keeps track of version numbers deployed. Commit all changes to this file.
- `functions/package.json` is used when checking function versions. 
- You should 

## Deploying

To deploy the functions to all projects, you first need to change the package version number at `functions/package.json`. This tells the deployment script that you have changes to push. Once done, you can then call:

```
node deploy
```

Each target is checked to see if their last-known deployed version number differs from the version number in this deployment. If they do differ, the functions are uploaded to Firebase. Each function overwrites any existing function of the same name. Any function that exists in the cloud but not in the local workspace will be deleted from the cloud.

After each successful deployment, remember to commit changes to `deploy_state.json`. This file is keeping track of the last successfully deployed version numbers, by project, so that the next deployment knows which projects are out-of-date.

### Optional Flags

To force all projects to be updated, regardless of version numbers, use `--force`:

```bash
node deploy --force
```

To see the output of `firebase-tools` commands, use `--verbose`:

```bash
node deploy --verbose
```

## Targets

When you start using a new project in Firebase, you'll need to register the project as a target of this deployment utility. To do this, open a terminal and navigate to the tools root directory and type: 

```bash
firebase use --add
```

You will be presented with an interactive interface for selecting the project. If the new project is not listed, ensure that the service account used for `firebase login` has administrator access to it. Give the project an alias and then commit changes to `.firebaserc`.

